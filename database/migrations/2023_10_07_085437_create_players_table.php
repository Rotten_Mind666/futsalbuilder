<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('players', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('surname');
            $table->string('foot');
            $table->integer('weakfoot');
            $table->integer('height');
            $table->integer('skill')->default(1);
            $table->integer('weight');
            $table->integer('age');

            $table->integer('as')->default(0);
            $table->integer('att')->default(0);
            $table->integer('ad')->default(0);
            $table->integer('coc')->default(0);
            $table->integer('cc')->default(0);
            $table->integer('es')->default(0);
            $table->integer('ed')->default(0);
            $table->integer('cdc')->default(0);
            $table->integer('ts')->default(0);
            $table->integer('dc')->default(0);
            $table->integer('td')->default(0);
            $table->integer('por')->default(0);

            $table->integer('accelerazione')->default(1);
            $table->integer('velocitascatto')->default(1);
            $table->integer('posizionamentoinattacco')->default(1);
            $table->integer('finalizzazione')->default(1);
            $table->integer('potenzatiro')->default(1);
            $table->integer('tirodalladistanza')->default(1);
            $table->integer('tirialvolo')->default(1);
            $table->integer('rigori')->default(1);
            $table->integer('visione')->default(1);
            $table->integer('cross')->default(1);
            $table->integer('precisionesupunizione')->default(1);
            $table->integer('passaggiocorto')->default(1);
            $table->integer('passaggiolungo')->default(1);
            $table->integer('effetto')->default(1);
            $table->integer('agilità')->default(1);
            $table->integer('equilibrio')->default(1);
            $table->integer('reattività')->default(1);
            $table->integer('controllopalla')->default(1);
            $table->integer('dribbling')->default(1);
            $table->integer('freddezza')->default(1);
            $table->integer('intercettazioni')->default(1);
            $table->integer('precisioneditesta')->default(1);
            $table->integer('letturadifensiva')->default(1);
            $table->integer('contrastoinpiedi')->default(1);
            $table->integer('scivolata')->default(1);
            $table->integer('elevazione')->default(1);
            $table->integer('resistenza')->default(1);
            $table->integer('forza')->default(1);
            $table->integer('aggressività')->default(1);

            $table->integer('overall')->default(1);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('players');
    }
};
