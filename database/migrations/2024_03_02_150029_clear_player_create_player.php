<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::drop('players');
        Schema::create('players', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('cognome')->nullable(true);
            $table->string('overall');
            $table->string('ruolo');
            $table->string('vel');
            $table->string('tir');
            $table->string('dif');
            $table->string('fis');
            $table->string('pas');
            $table->string('dri');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        //
    }
};
