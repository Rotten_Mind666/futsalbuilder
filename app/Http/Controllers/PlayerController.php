<?php

namespace App\Http\Controllers;

use App\Models\Player;
use App\Models\PlayerShare;
use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class PlayerController extends Controller
{
    public $team1 = [];
    public $team2 = [];
    public $maxcristianiperteam;
    public $maxcristianitotale;
    /**
     * Display a listing of the resource.
     */
    public function shuffleview()
    {
        return view('card.shuffle');
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('card.create');
    }

    public function store(Request $request)
    {
        $newPlayer = [
            'name' => $request->name,
            'cognome' => $request->cognome,
            'ruolo' => strtoupper($request->ruolo),
            'vel' => $request->velocita,
            'tir' => $request->tiro,
            'dif' => $request->difesa,
            'fis' => $request->fisico,
            'pas' => $request->passaggio,
            'dri' => $request->dribbling
        ];

        if (strtolower($newPlayer['ruolo']) == 'difensore') {
            $newPlayer['vel'] = $newPlayer['vel'];
            $newPlayer['tir'] = $newPlayer['tir'];
            $newPlayer['dif'] = $newPlayer['dif'];
            $newPlayer['fis'] = $newPlayer['fis'];
            $newPlayer['pas'] = $newPlayer['pas'];
            $newPlayer['dri'] = $newPlayer['dri'];
            $newPlayer['overall'] = ($newPlayer['vel'] * 77 + $newPlayer['tir'] * 50 + $newPlayer['dif'] * 92 + $newPlayer['fis'] * 85 + $newPlayer['pas'] * 73 + $newPlayer['dri'] * 71) / (77 + 50 + 73 + 71 + 92 + 85);
        } elseif (strtolower($newPlayer['ruolo']) == 'centrocampista') {
            $newPlayer['vel'] = $newPlayer['vel'];
            $newPlayer['tir'] = $newPlayer['tir'];
            $newPlayer['dif'] = $newPlayer['dif'];
            $newPlayer['fis'] = $newPlayer['fis'];
            $newPlayer['pas'] = $newPlayer['pas'];
            $newPlayer['dri'] = $newPlayer['dri'];
            $newPlayer['overall'] = ($newPlayer['vel'] * 71 + $newPlayer['tir'] * 70 + $newPlayer['dif'] * 72 + $newPlayer['fis'] * 75 + $newPlayer['pas'] * 93 + $newPlayer['dri'] * 78) / (71 + 70 + 93 + 78 + 72 + 75);
        } elseif (strtolower($newPlayer['ruolo']) == 'attaccante') {
            $newPlayer['vel'] = $newPlayer['vel'];
            $newPlayer['tir'] = $newPlayer['tir'];
            $newPlayer['dif'] = $newPlayer['dif'];
            $newPlayer['fis'] = $newPlayer['fis'];
            $newPlayer['pas'] = $newPlayer['pas'];
            $newPlayer['dri'] = $newPlayer['dri'];
            $newPlayer['overall'] = ($newPlayer['vel'] * 80 + $newPlayer['tir'] * 90 + $newPlayer['dif'] * 50 + $newPlayer['fis'] * 75 + $newPlayer['pas'] * 70 + $newPlayer['dri'] * 80) / (80 + 90 + 70 + 80 + 50 + 75);
        }

        $pl = Player::create($newPlayer);

        PlayerShare::create([
            'idplayer' => $pl->id,
            'idowner' => Auth::user()->id,
        ]);

        return redirect()
            ->route('home')
            ->with('success', 'Il giocatore e\' stato creato con successo!');
    }
    /**
     * Display the specified resource.
     */
    public function show(Player $player)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Request $request)
    {
        $newPlayer = [
            'name' => $request->name,
            'cognome' => $request->cognome,
            'ruolo' => strtoupper($request->ruolo),
            'vel' => $request->velocita,
            'tir' => $request->tiro,
            'dif' => $request->difesa,
            'fis' => $request->fisico,
            'pas' => $request->passaggio,
            'dri' => $request->dribbling
        ];

        if (strtolower($newPlayer['ruolo']) == 'difensore') {
            $newPlayer['vel'] = $newPlayer['vel'];
            $newPlayer['tir'] = $newPlayer['tir'];
            $newPlayer['dif'] = $newPlayer['dif'];
            $newPlayer['fis'] = $newPlayer['fis'];
            $newPlayer['pas'] = $newPlayer['pas'];
            $newPlayer['dri'] = $newPlayer['dri'];
            $newPlayer['overall'] = ($newPlayer['vel'] * 77 + $newPlayer['tir'] * 50 + $newPlayer['dif'] * 92 + $newPlayer['fis'] * 85 + $newPlayer['pas'] * 73 + $newPlayer['dri'] * 71) / (77 + 50 + 73 + 71 + 92 + 85);
        } elseif (strtolower($newPlayer['ruolo']) == 'centrocampista') {
            $newPlayer['vel'] = $newPlayer['vel'];
            $newPlayer['tir'] = $newPlayer['tir'];
            $newPlayer['dif'] = $newPlayer['dif'];
            $newPlayer['fis'] = $newPlayer['fis'];
            $newPlayer['pas'] = $newPlayer['pas'];
            $newPlayer['dri'] = $newPlayer['dri'];
            $newPlayer['overall'] = ($newPlayer['vel'] * 71 + $newPlayer['tir'] * 70 + $newPlayer['dif'] * 72 + $newPlayer['fis'] * 75 + $newPlayer['pas'] * 93 + $newPlayer['dri'] * 78) / (71 + 70 + 93 + 78 + 72 + 75);
        } elseif (strtolower($newPlayer['ruolo']) == 'attaccante') {
            $newPlayer['vel'] = $newPlayer['vel'];
            $newPlayer['tir'] = $newPlayer['tir'];
            $newPlayer['dif'] = $newPlayer['dif'];
            $newPlayer['fis'] = $newPlayer['fis'];
            $newPlayer['pas'] = $newPlayer['pas'];
            $newPlayer['dri'] = $newPlayer['dri'];
            $newPlayer['overall'] = ($newPlayer['vel'] * 80 + $newPlayer['tir'] * 90 + $newPlayer['dif'] * 50 + $newPlayer['fis'] * 75 + $newPlayer['pas'] * 70 + $newPlayer['dri'] * 80) / (80 + 90 + 70 + 80 + 50 + 75);
        }

        $isyourplayer = PlayerShare::where(['idplayer' => $request->idplay, 'idowner' => Auth::user()->id])->exists();

        if ($isyourplayer != null && $isyourplayer != false) {
            $pl = Player::where(['id' => $request->idplay])->update($newPlayer);

            return redirect()
                ->back()
                ->with('success', 'Il giocatore e\' stato modificato con successo! ' . strtoupper(explode(' ', Auth::user()->name)[0]) . ' TI AMO ❤️');
        } else {
            return redirect()
                ->back()
                ->with('success', 'Non puoi modificare un giocatore non tuo!!! l\'evento e\' stato registrato! Sarai punito per questo!');
        }
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Player $player)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function delete(Request $request)
    {
        $record = Player::findOrFail($request->id);
        $isyourplayer = PlayerShare::where(['idplayer' => $request->id, 'idowner' => Auth::user()->id])->exists();

        if ($isyourplayer != null && $isyourplayer != false) {
            $record->delete();
            return redirect()
                ->back()
                ->with('success', 'Ne hai fatto fuori uno... bravo continua cosi mi alleggerisci il database ❤️!');
        } else {
            return redirect()
                ->back()
                ->with('success', 'Non puoi modificare un giocatore non tuo!!! l\'evento e\' stato registrato! Sarai punito per questo!');
        }

        return redirect()->back()->with('success', 'Record eliminato con successo.');
    }

    function shuffle(Request $request)
    {
        $players = Player::whereIn('id', $request->squad)->get();

        $this->maxcristianiperteam = $request->maxcristiani;
        $this->maxcristianitotale = $request->maxcristiani * 2;
        $this->creaSquadre($players->toArray());
        $teams = ['team1' => $this->team1, 'team2' => $this->team2];

        return response()->json([
            'route' => route('card.shuffleview'),
            'teams' => json_encode($teams)
        ]);
    }

    function creaSquadre($giocatori)
    {
        // Dividi i giocatori in base al ruolo
        $centrocampisti = array_filter($giocatori, function ($giocatore) {
            return $giocatore['ruolo'] === 'CENTROCAMPISTA'; //5
        });

        $difensori = array_filter($giocatori, function ($giocatore) {
            return $giocatore['ruolo'] === 'DIFENSORE'; //3
        });

        $attaccanti = array_filter($giocatori, function ($giocatore) {
            return $giocatore['ruolo'] === 'ATTACCANTE'; //2
        });

        $this->organizeteam($centrocampisti, $difensori, $attaccanti, array_merge($centrocampisti, $difensori, $attaccanti));

        return [
            'squadra1' => $this->team1,
            'squadra2' => $this->team2,
        ];
    }

    private function organizeteam($c, $d, $a, $playerList)
    {
        shuffle($playerList);
        foreach ($playerList as $player) {
            $check = $this->checkRoleInArray();
            if ($player['ruolo'] == 'ATTACCANTE') {
                if (count($check['team1']['attaccanti']) == 0 && count($this->team1) < $this->maxcristianiperteam) {
                    array_push($this->team1, $player);
                } else {
                    if (((count($a) / 2) - count($check['team1']['attaccanti'])) >= 0  && count($this->team1) < $this->maxcristianiperteam) {
                        array_push($this->team1, $player);
                    } else {
                        array_push($this->team2, $player);
                    }
                }
            }
            if ($player['ruolo'] == 'CENTROCAMPISTA') {
                if (count($check['team1']['centrocampisti']) == 0 && count($this->team1) < $this->maxcristianiperteam) {
                    array_push($this->team1, $player);
                } else {
                    if (((count($c) / 2) - count($check['team1']['centrocampisti'])) >= 0 && count($this->team1) < $this->maxcristianiperteam) {
                        array_push($this->team1, $player);
                    } else {
                        array_push($this->team2, $player);
                    }
                }
            }
            if ($player['ruolo'] == 'DIFENSORE') {
                if (count($check['team1']['difensori']) == 0 && count($this->team1) < $this->maxcristianiperteam) {
                    array_push($this->team1, $player);
                } else {
                    if (((count($d) / 2) - count($check['team1']['difensori'])) >= 0 && count($this->team1) < $this->maxcristianiperteam) {
                        array_push($this->team1, $player);
                    } else {
                        array_push($this->team2, $player);
                    }
                }
            }
        }
        return true;
    }

    function checkRoleInArray()
    {
        $centrocampistiTeam1 = array_filter($this->team1, function ($giocatore) {
            return $giocatore['ruolo'] === 'CENTROCAMPISTA'; //5
        });

        $difensoriTeam1 = array_filter($this->team1, function ($giocatore) {
            return $giocatore['ruolo'] === 'DIFENSORE'; //3
        });

        $attaccantiTeam1 = array_filter($this->team1, function ($giocatore) {
            return $giocatore['ruolo'] === 'ATTACCANTE'; //2
        });

        $centrocampistiTeam2 = array_filter($this->team2, function ($giocatore) {
            return $giocatore['ruolo'] === 'CENTROCAMPISTA'; //5
        });

        $difensoriTeam2 = array_filter($this->team2, function ($giocatore) {
            return $giocatore['ruolo'] === 'DIFENSORE'; //3
        });

        $attaccantiTeam2 = array_filter($this->team2, function ($giocatore) {
            return $giocatore['ruolo'] === 'ATTACCANTE'; //2
        });

        return [
            'team1' => [
                'centrocampisti' => $centrocampistiTeam1,
                'difensori' => $difensoriTeam1,
                'attaccanti' => $attaccantiTeam1
            ],
            'team2' => [
                'centrocampisti' => $centrocampistiTeam2,
                'difensori' => $difensoriTeam2,
                'attaccanti' => $attaccantiTeam2
            ],
        ];
    }
    public function myaccount()
    {
        $myplayer = PlayerShare::where('idowner', Auth::user()->id)->pluck('idplayer')->toArray();
        $pl = Player::whereIn('id', $myplayer)->get();

        return view('myaccount', compact('pl'));
    }

    public function sharewith(Request $request)
    {
        foreach ($request->shareowner as $s) {
            foreach ($request->sharedlist as $p) {

                $checkpl = PlayerShare::where(['idowner' => $s, 'idplayer' => $p])->get();
                if ($checkpl->count() === 0) {
                    PlayerShare::create([
                        'idowner' => $s,
                        'idplayer' => $p
                    ]);
                }
            }
        }

        return response()->json('200');
    }
}
