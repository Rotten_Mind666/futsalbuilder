<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Player extends Model
{
    use HasFactory;

    public $table = 'players';

    protected $fillable = [
        // 'name',
        // 'surname',
        // 'foot',
        // 'weakfoot',
        // 'height',
        // 'skill',
        // 'weight',
        // 'age',
        // 'as',
        // 'att',
        // 'ad',
        // 'coc',
        // 'cc',
        // 'es',
        // 'ed',
        // 'cdc',
        // 'ts',
        // 'dc',
        // 'td',
        // 'por',
        // 'accelerazione',
        // 'velocitascatto',
        // 'posizionamentoinattacco',
        // 'finalizzazione',
        // 'potenzatiro',
        // 'tirodalladistanza',
        // 'tirialvolo',
        // 'rigori',
        // 'visione',
        // 'cross',
        // 'precisionesupunizione',
        // 'passaggiocorto',
        // 'passaggiolungo',
        // 'effetto',
        // 'agilità',
        // 'equilibrio',
        // 'reattività',
        // 'controllopalla',
        // 'dribbling',
        // 'freddezza',
        // 'intercettazioni',
        // 'precisioneditesta',
        // 'letturadifensiva',
        // 'contrastoinpiedi',
        // 'scivolata',
        // 'elevazione',
        // 'resistenza',
        // 'forza',
        // 'aggressività',
        // 'overall'
        'name',
        'cognome',
        'overall',
        'ruolo',
        'vel',
        'tir',
        'dif',
        'fis',
        'pas',
        'dri',
        'img'
    ];
}
