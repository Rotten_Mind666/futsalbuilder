<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PlayerShare extends Model
{
    use HasFactory;
    
    public $table = 'playershare';

    public $fillable = [
        'idplayer',
        'idowner'
    ];
}
