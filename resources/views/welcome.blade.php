@php
    use App\Models\Player;
@endphp
@extends('layouts.app')
@section('css')
    @parent
    <style>
        .bg-glass {
            background: rgba(255, 255, 255, 0.2);
            box-shadow: 0 4px 30px rgba(0, 0, 0, 0.1);
            backdrop-filter: blur(5px);
            -webkit-backdrop-filter: blur(5px);
            border: 1px solid rgba(255, 255, 255, 0.3);
        }

        .footballer-card {
            background: rgba(255, 255, 255, 0.3);
            border-radius: 20px;
        }

        .insquad {
            max-width: 20rem;
        }


        #loadingScreen {
            display: none;
            position: fixed;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            background-color: rgba(0, 0, 0, 0.5);
            justify-content: center;
            align-items: center;
            z-index: 999;
        }

        .loader {
            border: 8px solid #f3f3f3;
            border-top: 8px solid #3498db;
            border-radius: 50%;
            width: 50px;
            height: 50px;
            animation: spin 1s linear infinite;
        }

        @keyframes spin {
            0% {
                transform: rotate(0deg);
            }

            100% {
                transform: rotate(360deg);
            }
        }
    </style>
@endsection
@section('content')
    <div class="container-fluid px-5">
        <div class="row">
            <div class="col-8 bg-glass rounded-start text-white p-4"
                style="max-height: 85vh; min-height: 85vh; overflow-x: hidden !important;">
                <div class="row align-items-center">
                    <div class="col-6">
                        <select name="squadnumber" id="squadnumber" class="form-control" onchange="checkPartecipanti()">
                            <option value="5">5 vs 5</option>
                            <option value="6">6 vs 6</option>
                            <option value="7">7 vs 7</option>
                            <option value="8">8 vs 8</option>
                        </select>
                    </div>
                    <div class="col-3">
                        <span>Giocatori mancanti: <span class="ngiocatorimancanti">10</span></span>
                    </div>
                    <div class="col-3">
                        <button class="btn btn-primary" onclick="shuffle()">RANDOMIZZA</button>
                    </div>
                </div>
                <div id="teamzerocontainer" class="row justify-content-around">

                </div>
            </div>
            <div id="panchina" class="col-4 bg-white rounded-end overflow-scroll"
                style="max-height: 85vh; min-height: 85vh; overflow-x: hidden !important;">
                <p>Lista giocatori:</p>
                <input type="text" class="searchplayers form-control" placeholder="Cerca giocatore...">
                @if (isset($pl))
                    @foreach ($pl as $pla)
                        <div class="card my-2 cliccabile" idplayer="{{ $pla->id }}">
                            <div class="card-body">
                                <h5 class="card-title text-uppercase nomeplayer">
                                    {{ $pla->name . ' ' . $pla->cognome . ' ' . round($pla->overall, 0) }}</h5>
                                <h6 class="card-subtitle mb-2 text-muted text-capitalize">
                                    {{ $pla->ruolo }}
                                    @php
                                        $l = [$pla->vel, $pla->tir, $pla->pas, $pla->dri, $pla->dif, $pla->fis];
                                        // Converte le stringhe in numeri interi
                                        $numericValues = array_map('intval', $l);

                                        // Trova il valore più grande e la sua posizione
                                        $maxValue = max($numericValues);
                                        $position = array_search($maxValue, $numericValues);

                                        $l[$position] = $l[$position] . '*';

                                    @endphp
                                    @if ($pla->ruolo == 'DIFENSORE')
                                        <i class="fa-solid fa-shield text-primary"></i>
                                    @elseif ($pla->ruolo == 'CENTROCAMPISTA')
                                        <i class="fa-solid fa-pen-ruler text-success"></i>
                                    @else
                                        <i class="fa-solid fa-up-long text-danger"></i>
                                    @endif
                                </h6>
                                <div class="card-text row">
                                    <div class="col-6">
                                        <p class="m-0">Velocita: {{ str_contains($l[0], '*') ? $l[0] . '🔥' : $l[0] }}
                                        </p>
                                        <p class="m-0">Tiro: {{ str_contains($l[1], '*') ? $l[1] . '🔥' : $l[1] }}</p>
                                        <p class="m-0">Passaggio: {{ str_contains($l[2], '*') ? $l[2] . '🔥' : $l[2] }}
                                        </p>
                                    </div>
                                    <div class="col-6">
                                        <p class="m-0">Dribbling: {{ str_contains($l[3], '*') ? $l[3] . '🔥' : $l[3] }}
                                        </p>
                                        <p class="m-0">Difesa: {{ str_contains($l[4], '*') ? $l[4] . '🔥' : $l[4] }}</p>
                                        <p class="m-0">Fisico: {{ str_contains($l[5], '*') ? $l[5] . '🔥' : $l[5] }}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>
    </div>

    <div id="loadingScreen">
        <div class="loader"></div>
    </div>
@endsection
@section('scripts')
    @parent
    <script>
        var partecipanti = [];
        $('.searchplayers').on('keyup', function() {
            if ($(this).val().length > 0) {
                var searchTerm = $(this).val().toLowerCase();
                $('.nomeplayer').parent().parent().hide();
                $('.nomeplayer').filter(function() {
                    return $(this).text().toLowerCase().includes(searchTerm);
                }).parent().parent().show();
            } else {
                $('.nomeplayer').parent().parent().show();
            }
        });

        $('.cliccabile').on('click', function() {
            if ($(this).hasClass('insquad')) {
                var index = partecipanti.indexOf($(this).attr('idplayer'));
                if (index > -1) {
                    partecipanti.splice(index, 1);
                }

                $(this).removeClass('insquad');
                $(this).appendTo('#panchina');
            } else {
                partecipanti.push($(this).attr('idplayer'));

                $(this).addClass('insquad');
                $(this).prependTo('#teamzerocontainer');
            }
            var canadd = checkPartecipanti();

        });


        function checkPartecipanti() {
            var maxnumpartecipanti = $('#squadnumber').val() * 2;
            var numpartecipanti = partecipanti.length;

            if ((maxnumpartecipanti - numpartecipanti) >= 0) {
                $('.ngiocatorimancanti').text(maxnumpartecipanti - numpartecipanti);
            } else {
                Swal.fire({
                    title: 'Errore!',
                    text: 'Numero partecipanti massimo superato, ho provveduto a ripristinare la situazione!',
                    icon: 'error',
                    confirmButtonText: 'Ok'
                });
                if (numpartecipanti !== maxnumpartecipanti) {
                    for (let index = maxnumpartecipanti; index < numpartecipanti; index++) {
                        $('[idplayer="' + partecipanti[partecipanti.length - 1] + '"]').removeClass('insquad').appendTo(
                            '#panchina');
                        partecipanti.pop();
                    }
                }
            }
        }

        function shuffle() {
            var maxnumpartecipanti = $('#squadnumber').val() * 2;
            var numpartecipanti = partecipanti.length;
            if (maxnumpartecipanti - numpartecipanti == 0) {
                showLoadingScreen();

                $.ajax({
                    url: "{{ route('shuffle') }}",
                    data: {
                        'squad': partecipanti,
                        'maxcristiani': $('#squadnumber').val()
                    },
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type: 'POST',
                    dataType: 'json',
                    success: function(result) {
                        setCookie('teams', result.teams, 9999);
                        location.href = result.route;
                    }
                });
            } else {
                Swal.fire({
                    title: 'Errore!',
                    text: 'Controllare bene se tutti i giocatori sono presenti',
                    icon: 'error',
                    confirmButtonText: 'Ok'
                });
            }
        }

        function showLoadingScreen() {
            document.getElementById('loadingScreen').style.display = 'flex';

            // Simula un'attività di caricamento (puoi sostituire questo timeout con la tua logica di caricamento)
            setTimeout(function() {
                hideLoadingScreen();
            }, 5000);
        }

        function hideLoadingScreen() {
            document.getElementById('loadingScreen').style.display = 'none';
        }
    </script>
@endsection
