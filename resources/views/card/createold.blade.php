@extends('layouts.app')
@section('css')
    @parent
    <style>
        .wrapper {
            top: 50%;
            left: 15%;
            transform: scale(1);
            margin: 50px 0px;
            position: relative;
        }
        .wrapper{
            transform: scale(0.8) !important;
        }
    </style>
@endsection
@section('content')
    @if (session()->has('success'))
        <div class="alert alert-success">
            {{ session()->get('success') }}
        </div>
    @endif
    <div class="container-fluid pt-5 formcreate">
        <div class="row align-items-center">
            <div class="col-3">
                <div class="wrapper">
                    <div class="fut-player-card">
                        <!-- Player Card Top -->
                        <div class="player-card-top">
                            <div class="player-master-info">
                                <div class="player-rating">
                                    <span id="overallnumber">1</span>
                                </div>
                            </div>
                            <div class="player-picture">
                                <input class="uploadimage d-none" type="file" name="" id="">
                                <img src="{{ asset('addimageplayer.png') }}" alt="add" class="addimageplayer"
                                    draggable="false"
                                    onclick="alert('in fase di sviluppo'); /*$('.uploadimage').click();*/">
                            </div>
                        </div>
                        <!-- Player Card Bottom -->
                        <div class="player-card-bottom">
                            <div class="player-info">
                                <!-- Player Name -->
                                <div class="player-name py-2">
                                    <span class="d-flex">
                                        <input type="text" class="form-control w-50" id="name" placeholder="Nome"
                                            name="name">
                                        <input type="text" class="form-control w-50" id="surname" placeholder="Cognome"
                                            name="surname">
                                    </span>
                                </div>
                                <!-- Player Features -->
                                <div class="player-features">
                                    <div class="player-features-col d-flex mx-0 px-0 w-100 justify-content-between">
                                        <div class="text-center">
                                            <div class="player-feature-title">VEL</div>
                                            <div class="player-feature-value VELattr">1</div>
                                        </div>
                                        <div class="text-center">
                                            <div class="player-feature-title">TIR</div>
                                            <div class="player-feature-value TIRattr">1</div>
                                        </div>
                                        <div class="text-center">
                                            <div class="player-feature-title">PAS</div>
                                            <div class="player-feature-value PASattr">1</div>
                                        </div>
                                        <div class="text-center">
                                            <div class="player-feature-title">DRI</div>
                                            <div class="player-feature-value DRIattr">1</div>
                                        </div>
                                        <div class="text-center">
                                            <div class="player-feature-title">DIF</div>
                                            <div class="player-feature-value DIFattr">1</div>
                                        </div>
                                        <div class="text-center">
                                            <div class="player-feature-title">FIS</div>
                                            <div class="player-feature-value FISattr">1</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="player-features">
                                    <div class="player-features-col">
                                        <span>
                                            <img style="width: 20px !important; cursor: pointer;" foot="SX"
                                                class="footchose" src="{{ asset('footsx.png') }}" alt="">
                                            <img style="width: 20px !important; cursor: pointer;" foot="DX"
                                                class="footchose" src="{{ asset('footdx.png') }}" alt="">
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-2">
                <p class="text-white px-2">Eta': <input class="form-control" type="number" name="eta" id="eta">
                </p>
                <p class="text-white px-2">Altezza (CM): <input class="form-control" type="number" name="altezza"
                        id="altezza" placeholder="Es. 180"></p>
                <p class="text-white px-2">Peso (KG): <input class="form-control" type="number" name="peso"
                        id="peso" placeholder="Es. 70"></p>

                <table class="table table-dark rounded customtablechoserole">
                    <tbody>
                        <tr>
                            <td class="rolechosecell" roles="as" valore="0" rowspan="2">ES</td>
                            <td class="rolechosecell" roles="att" valore="0">ATT</td>
                            <td class="rolechosecell" roles="ad" valore="0" rowspan="2">AD</td>
                        </tr>
                        <tr>
                            <td class="rolechosecell" roles="coc" valore="0">COC</td>
                        </tr>
                        <tr>
                            <td class="rolechosecell" roles="es" valore="0" rowspan="2">ES</td>
                            <td class="rolechosecell" roles="cc" valore="0">CC</td>
                            <td class="rolechosecell" roles="ed" valore="0" rowspan="2">ED</td>
                        </tr>
                        <tr>
                            <td class="rolechosecell" roles="cdc" valore="0">CDC</td>
                        </tr>
                        <tr>
                            <td class="rolechosecell" roles="ts" valore="0">TS</td>
                            <td class="rolechosecell" roles="dc" valore="0">DC</td>
                            <td class="rolechosecell" roles="td" valore="0">TD</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td class="rolechosecell" roles="por" valore="0">POR</td>
                            <td></td>
                        </tr>
                    </tbody>
                </table>
                <p class="text-white">Piede debole:
                    <span class="star-rating">
                        <label class="weaklabel" for="rate-1" style="--i:1; cursor: pointer;">
                            <i class="fa-solid fa-star weakstar"></i>
                        </label>
                        <input type="radio" name="rating" id="rate-1" value="1" checked>
                        <label class="weaklabel" for="rate-2" style="--i:2; cursor: pointer;">
                            <i class="fa-solid fa-star weakstar"></i>
                        </label>
                        <input type="radio" name="rating" id="rate-2" value="2">
                        <label class="weaklabel" for="rate-3" style="--i:3; cursor: pointer;">
                            <i class="fa-solid fa-star weakstar"></i>
                        </label>
                        <input type="radio" name="rating" id="rate-3" value="3">
                        <label class="weaklabel" for="rate-4" style="--i:4; cursor: pointer;">
                            <i class="fa-solid fa-star weakstar"></i>
                        </label>
                        <input type="radio" name="rating" id="rate-4" value="4">
                        <label class="weaklabel" for="rate-5" style="--i:5; cursor: pointer;">
                            <i class="fa-solid fa-star weakstar"></i>
                        </label>
                        <input type="radio" name="rating" id="rate-5" value="5">
                    </span>
                </p>
                <p class="text-white">Mosse abilita:
                    <span class="star-skill">
                        <label class="skilllabel" for="skill-1" style="--i:1; cursor: pointer;">
                            <i class="fa-solid fa-star iconskill"></i>
                        </label>
                        <input type="radio" name="rating2" id="skill-1" value="1" checked>
                        <label class="skilllabel" for="skill-2" style="--i:2; cursor: pointer;">
                            <i class="fa-solid fa-star iconskill"></i>
                        </label>
                        <input type="radio" name="rating2" id="skill-2" value="2">
                        <label class="skilllabel" for="skill-3" style="--i:3; cursor: pointer;">
                            <i class="fa-solid fa-star iconskill"></i>
                        </label>
                        <input type="radio" name="rating2" id="skill-3" value="3">
                        <label class="skilllabel" for="skill-4" style="--i:4; cursor: pointer;">
                            <i class="fa-solid fa-star iconskill"></i>
                        </label>
                        <input type="radio" name="rating2" id="skill-4" value="4">
                        <label class="skilllabel" for="skill-5" style="--i:5; cursor: pointer;">
                            <i class="fa-solid fa-star iconskill"></i>
                        </label>
                        <input type="radio" name="rating2" id="skill-5" value="5">
                    </span>
                </p>
            </div>
            <div class="col-7">
                <div class="row text-white">
                    <div class="col-2 text-center">
                        <h3 class="fw-bold">VELOCITA</h3>
                        <div class="semi-donut margin" style="--percentage : 80; --fill: #FF3D00 ;">
                            80%
                        </div>
                        <p>Accelerazione <input type="number" name="accelerazione" value="1" min="1"
                                max="99" class="form-control"> </p>
                        <p>Velocita scatto <input type="number" name="velocitascatto" value="1" min="1"
                                max="99" class="form-control"> </p>
                    </div>
                    <div class="col-2 text-center">
                        <h3 class="fw-bold">TIRO</h3>
                        <div class="semi-donut margin" style="--percentage : 80; --fill: #FF3D00 ;">
                            80%
                        </div>
                        <p>Pos. in attacco <input type="number" name="posizionamentoinattacco" value="1"
                                min="1" max="99" class="form-control"> </p>
                        <p>Finalizzazione <input type="number" name="finalizzazione" value="1" min="1"
                                max="99" class="form-control"> </p>
                        <p>Potenza tiro <input type="number" name="potenzatiro" value="1" min="1"
                                max="99" class="form-control"> </p>
                        <p>Tiro dalla distanza <input type="number" name="tirodalladistanza" value="1"
                                min="1" max="99" class="form-control"> </p>
                        <p>Tiro al volo <input type="number" name="tirialvolo" value="1" min="1"
                                max="99" class="form-control"> </p>
                        <p>Rigori <input type="number" name="rigori" value="1" min="1" max="99"
                                class="form-control"> </p>
                    </div>
                     
                    <div class="col-2 text-center">
                        <h3 class="fw-bold">DRIBBLING</h3>
                        <div class="semi-donut margin" style="--percentage : 80; --fill: #FF3D00 ;">
                            80%
                        </div>
                        <P>Agilita <input type="number" name="agilità" value="1" min="1" max="99"
                                class="form-control"> </P>
                        <P>Equilibrio <input type="number" name="equilibrio" value="1" min="1"
                                max="99" class="form-control"> </P>
                        <P>Reattività <input type="number" name="reattività" value="1" min="1"
                                max="99" class="form-control"> </P>
                        <P>Controllo palla <input type="number" name="controllopalla" value="1" min="1"
                                max="99" class="form-control"> </P>
                        <P>Dribbling <input type="number" name="dribbling" value="1" min="1"
                                max="99" class="form-control"> </P>
                        <P>Freddezza <input type="number" name="freddezza" value="1" min="1"
                                max="99" class="form-control"> </P>
                    </div>
                    <div class="col-2 text-center">
                        <h3 class="fw-bold">DIFESA</h3>
                        <div class="semi-donut margin" style="--percentage : 80; --fill: #FF3D00 ;">
                            80%
                        </div>
                        <p>Intercettazioni <input type="number" name="intercettazioni" value="1" min="1"
                                max="99" class="form-control"> </p>
                        <p>Precisione di testa <input type="number" name="precisioneditesta" value="1"
                                min="1" max="99" class="form-control"> </p>
                        <p>Lettura difensiva <input type="number" name="letturadifensiva" value="1" min="1"
                                max="99" class="form-control"> </p>
                        <p>Contrasto in piedi <input type="number" name="contrastoinpiedi" value="1"
                                min="1" max="99" class="form-control"> </p>
                        <p>Scivolata <input type="number" name="scivolata" value="1" min="1"
                                max="99" class="form-control"> </p>
                    </div>
                    <div class="col-2 text-center">
                        <h3 class="fw-bold">FISICO</h3>
                        <div class="semi-donut margin" style="--percentage : 80; --fill: #FF3D00 ;">
                            80%
                        </div>
                        <p>Elevazione <input type="number" name="elevazione" value="1" min="1"
                                max="99" class="form-control"> </p>
                        <p>Resistenza <input type="number" name="resistenza" value="1" min="1"
                                max="99" class="form-control"> </p>
                        <p>Forza <input type="number" name="forza" value="1" min="1" max="99"
                                class="form-control"> </p>
                        <p>aggressività <input type="number" name="aggressività" value="1" min="1"
                                max="99" class="form-control"> </p>
                    </div>
                </div>
            </div>
            <div class="col-12 text-center bg-white py-4 mt-5">
                <button class="btn btn-lg btn-dark" onclick="saveData()">CREA GIOCATORE</button>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    @parent
    <script>
        var playerInfo = {};
        var rolevalue = [];
        rolevalue[0] = [
            'as',
            'att',
            'ad',
            'coc',
            'es',
            'cc',
            'ed',
            'cdc',
            'ts',
            'dc',
            'td',
            'por'
        ];
        rolevalue[1] = [];
        rolevalue[2] = [];
        rolevalue[3] = [];

        $(document).ready(function() {
            $('input[type="number"]').on('input', function() {
                if (typeof rolevalue[3][0] != 'undefined') {
                    if (parseInt($(this).val()) > parseInt($(this).prop('max'))) {
                        $(this).val(99);
                    }
                    playerInfo[$(this).attr('name')] = $(this).val();
                    calc();
                } else {
                    swa('Imposta un ruolo principale');
                    $(this).val(1);
                }
            });

            $('.footchose').on('click', function() {
                $('.footchose').removeClass('selected');
                $(this).addClass('selected');
                playerInfo['foot'] = $(this).attr('foot');
            });

            $('.rolechosecell').on('click', function() {
                let levelarray = rolevalue[$(this).attr('valore')];
                let keyRole = levelarray.indexOf($(this).attr('roles'));
                if (keyRole !== -1) {
                    if ($(this).attr('valore') == 0) {
                        $(this).attr('valore', 1);
                        $(this).removeClass('rolechosecell0');
                        $(this).addClass('rolechosecell1');
                        delete rolevalue[0][keyRole];
                        rolevalue[1].push($(this).attr('roles'));
                        cleanArray();
                    } else if ($(this).attr('valore') == 1) {
                        $(this).attr('valore', 2);
                        $(this).removeClass('rolechosecell1');
                        $(this).addClass('rolechosecell2');
                        delete rolevalue[1][keyRole];
                        rolevalue[2].push($(this).attr('roles'));
                        cleanArray();
                    } else if ($(this).attr('valore') == 2) {
                        $(this).attr('valore', 3);
                        $(this).removeClass('rolechosecell2');
                        $(this).addClass('rolechosecell3');
                        delete rolevalue[2][keyRole];
                        rolevalue[3][0] = $(this).attr('roles');
                        cleanArray();
                    } else if ($(this).attr('valore') == 3) {
                        $(this).attr('valore', 0);
                        $(this).removeClass('rolechosecell3');
                        delete rolevalue[3][0];
                        rolevalue[0].push($(this).attr('roles'));
                        cleanArray();
                    }
                }
            });
        });

        function cleanArray() {
            rolevalue[1].filter(function(v) {
                return v !== ''
            });
            rolevalue[2].filter(function(v) {
                return v !== ''
            });
            rolevalue[3].filter(function(v) {
                return v !== ''
            });
            rolevalue[0].filter(function(v) {
                return v !== ''
            });

        }

        function v(e) {
            return $('input[name="' + e + '"]').val()
        }

        function calc() {
            var a = 0,
                i = 0,
                p = rolevalue[3][0],
                k = $('#overallnumber'),
                o = k.val(),
                sw = ["1"],
                rwb = ["2", "8"],
                rb = ["3", "7"],
                rcb = ["4", "5", "6"],
                rdm = ["9", "10", "11"],
                rm = ["12", "16"],
                rcm = ["13", "14", "15"],
                ram = ["17", "18", "19"],
                rf = ["20", "21", "22"],
                rw = ["23", "27"],
                rs = ["24", "25", "26"];

            a = v('elevazione') * 0.04 + v('forza') * 0.10 + v('reattività') * 0.05 + v('aggressività') * 0.08 + v(
                    'intercettazioni') * 0.08 + v('controllopalla') * 0.05 + v('precisioneditesta') * 0.10 + v(
                    'passaggiocorto') * 0.05 + v(
                    'letturadifensiva') * 0.15 + v('contrastoinpiedi') * 0.15 + v('scivolata') * 0.15, a = Math.round(a),
                i = ir(1,
                    a), o = sw.indexOf(p) != -1 ? a + +i : o,

                a = v('accelerazione') * 0.04 + v('velocitascatto') * 0.06 + v('resistenza') * 0.10 + v('reattività') *
                0.08 + v(
                    'intercettazioni') * 0.12 + v('controllopalla') * 0.08 + v('cross') * 0.12 + v('dribbling') * 0.04 + v(
                    'passaggiocorto') * 0.10 + v('letturadifensiva') * 0.07 + v('contrastoinpiedi') * 0.08 + v(
                    'scivolata') * 0.11,
                a = Math.round(a), i = ir(2, a), o = rwb.indexOf(p) != -1 ? a + +i :
                o,

                a = v('accelerazione') * 0.05 + v('velocitascatto') * 0.07 + v('resistenza') * 0.08 + v('reattività') *
                0.08 + v(
                    'intercettazioni') * 0.12 + v('controllopalla') * 0.07 + v('cross') * 0.09 + v('precisioneditesta') *
                0.04 + v(
                    'passaggiocorto') * 0.07 + v('letturadifensiva') * 0.08 + v('contrastoinpiedi') * 0.11 + v(
                    'scivolata') * 0.14,
                a = Math.round(a), i = ir(3, a), o = rb
                .indexOf(p) != -1 ? a + +i : o,

                a = v('velocitascatto') * 0.02 + v('elevazione') * 0.03 + v('forza') * 0.10 + v('reattività') * 0.05 + v(
                    'aggressività') * 0.07 + v('intercettazioni') * 0.13 + v('controllopalla') * 0.04 + v(
                    'precisioneditesta') * 0.10 + v(
                    'passaggiocorto') * 0.05 + v('letturadifensiva') * 0.14 + v('contrastoinpiedi') * 0.17 + v(
                    'scivolata') * 0.10,
                a = Math.round(a), i = ir(4, a), o = rcb
                .indexOf(p) != -1 ? a + +i : o,

                a = v('resistenza') * 0.06 + v('forza') * 0.04 + v('reattività') * 0.07 + v('aggressività') * 0.05 + v(
                    'intercettazioni') * 0.14 + v('visione') * 0.04 + v('controllopalla') * 0.10 + v('passaggiolungo') *
                0.10 + v(
                    'passaggiocorto') * 0.14 + v('letturadifensiva') * 0.09 + v('contrastoinpiedi') * 0.12 + v(
                    'scivolata') * 0.05,
                a = Math.round(a), i = ir(9, a), o = rdm
                .indexOf(p) != -1 ? a + +i : o,

                a = v('accelerazione') * 0.07 + v('velocitascatto') * 0.06 + v('resistenza') * 0.05 + v('reattività') *
                0.07 + v(
                    'posizionamentoinattacco') * 0.08 + v('visione') * 0.07 + v('controllopalla') * 0.13 + v('cross') *
                0.10 + v(
                    'dribbling') * 0.15 + v('finalizzazione') * 0.06 + v('passaggiolungo') * 0.05 + v('passaggiocorto') *
                0.11, a =
                Math.round(a), i = ir(12, a), $(".rm"), o = rm
                .indexOf(p) != -1 ? a + +i : o,

                a = v('resistenza') * 0.06 + v('reattività') * 0.08 + v('intercettazioni') * 0.05 + v(
                    'posizionamentoinattacco') * 0.06 + v(
                    'visione') * 0.13 + v('controllopalla') * 0.14 + v('dribbling') * 0.07 + v('finalizzazione') * 0.02 + v(
                    'passaggiolungo') * 0.13 + v('passaggiocorto') * 0.17 + v('tirodalladistanza') * 0.04 + v(
                    'contrastoinpiedi') * 0.05,
                a = Math.round(a), i = ir(13, a), $(".rcm"), o = rcm
                .indexOf(p) != -1 ? a + +i : o,

                a = v('accelerazione') * 0.04 + v('velocitascatto') * 0.03 + v('agilità') * 0.03 + v('reattività') * 0.07 +
                v(
                    'posizionamentoinattacco') * 0.09 + v('visione') * 0.14 + v('controllopalla') * 0.15 + v('dribbling') *
                0.13 + v(
                    'finalizzazione') * 0.07 + v('passaggiolungo') * 0.04 + v('passaggiocorto') * 0.16 + v(
                    'tirodalladistanza') * 0.05, a =
                Math.round(a), i = ir(17, a), o = ram
                .indexOf(p) != -1 ? a + +i : o,

                a = v('accelerazione') * 0.05 + v('velocitascatto') * 0.05 + v('reattività') * 0.09 + v(
                    'posizionamentoinattacco') * 0.13 +
                v('visione') * 0.08 + v('controllopalla') * 0.15 + v('dribbling') * 0.14 + v('finalizzazione') * 0.11 + v(
                    'precisioneditesta') * 0.02 + v('passaggiocorto') * 0.09 + v('potenzatiro') * 0.05 + v(
                    'tirodalladistanza') * 0.04, a = Math
                .round(a), i = ir(20, a), $(".rf"), o = rf.indexOf(
                    p) != -1 ? a + +i : o,

                a = v('accelerazione') * 0.07 + v('velocitascatto') * 0.06 + v('agilità') * 0.03 + v('reattività') * 0.07 +
                v(
                    'posizionamentoinattacco') * 0.09 + v('visione') * 0.06 + v('controllopalla') * 0.14 + v('cross') *
                0.09 + v(
                    'dribbling') * 0.16 + v('finalizzazione') * 0.10 + v('passaggiocorto') * 0.09 + v('tirodalladistanza') *
                0.04, a = Math
                .round(a), i = ir(23, a), $(".rw"), o = rw.indexOf(
                    p) != -1 ? a + +i : o,

                a = v('accelerazione') * 0.04 + v('velocitascatto') * 0.05 + v('forza') * 0.05 + v('reattività') * 0.08 + v(
                    'posizionamentoinattacco') * 0.13 + v('controllopalla') * 0.10 + v('dribbling') * 0.07 + v(
                    'finalizzazione') * 0.18 + v(
                    'precisioneditesta') * 0.10 + v('passaggiocorto') * 0.05 + v('potenzatiro') * 0.10 + v(
                    'tirodalladistanza') * 0.03 + v(
                    'tirialvolo') * 0.02, a = Math.round(a), i = ir(24, a), o = rs.indexOf(p) != -1 ? a + +i : o;
            //o = overall arrotondato
            $('#overallnumber').text(a);

            console.log(v('accelerazione'), v('velocitascatto'));
            $('.VELattr').text((parseInt(v('accelerazione')) + parseInt(v('velocitascatto'))).toFixed(2) / 2);
            $('.TIRattr').text(
                ((parseInt(v('posizionamentoinattacco')) + parseInt(v('finalizzazione')) + parseInt(v('potenzatiro')) +
                    parseInt(v('tirodalladistanza')) + parseInt(v('tirialvolo')) + parseInt(v('rigori'))) / 6).toFixed(
                    0)
            );
            $('.PASattr').text(
                ((parseInt(v('visione')) + parseInt(v('cross')) + parseInt(v('precisionesupunizione')) + parseInt(v(
                    'passaggiocorto')) + parseInt(v('passaggiolungo')) + parseInt(v('effetto'))) / 6).toFixed(0)
            );
            $('.DRIattr').text(
                ((parseInt(v('agilità')) + parseInt(v('equilibrio')) + parseInt(v('reattività')) + parseInt(v(
                    'controllopalla')) + parseInt(v('dribbling')) + parseInt(v('freddezza'))) / 6).toFixed(0)
            );
            $('.DIFattr').text(
                ((parseInt(v('intercettazioni')) + parseInt(v('precisioneditesta')) + parseInt(v('letturadifensiva')) +
                    parseInt(v('contrastoinpiedi')) + parseInt(v('scivolata'))) / 5).toFixed(0)
            );
            $('.FISattr').text(
                ((parseInt(v('elevazione')) + parseInt(v('resistenza')) + parseInt(v('forza')) + parseInt(v(
                    'aggressività'))) / 4).toFixed(0)
            );
        }

        function ir(p, o) {
            //t e' uguale al potenziale max 99
            //p e' uguale al id del ruolo selezionato (menu a tendina)
            //p == 20 || p == 21 || p == 22 || p == 23 || p == 27 i numeri fanno riferimento al centrocampo (primo screen che ti ho mandato)
            //i e' uguale alla reputazione internazionale range da 1 a 5
            // quindi f sara' uguale a 2 o 3 perche (i > 2 ? 3 : 2) (se i e' maggiore di 2 allora sara' uguale a 3 altrimenti sara' uguale a 2)
            //o e' il valore arrotondato su questa base
            //let a = 88 * 0.04 + 88 * 0.05 + 55 * 0.05 + 34 * 0.08 + 66 * 0.13 + 88 * 0.10 + 34 * 0.07 + 66 * 0.18 + 77 * 0.10 + 77 * 0.05 + 44 * 0.10 + 22 * 0.03 + 55 * 0.02;
            //sra uguale a 63
            //Alla fine ritorna che se (magari 3 e' maggiore di 99 - 63) allora 99 -63 altrimenti [2 o 3]

            var t = v('pt'),
                i = $('select[name="ir"]').val(),
                f = (p == 20 || p == 21 || p == 22 || p == 23 || p == 27) ? 0 : (i > 2 ? 3 : 2);
            return f > (t - o) ? t - o : f;
        }

        function saveData() {
            if ($('#name').val().length <= 0) {
                swa('Impostare nome giocatore');
                return false;
            }
            if ($('#surname').val().length <= 0) {
                swa('Impostare cognome giocatore');
                return false;
            }
            if ($('#eta').val().length <= 0) {
                swa('Impostare eta');
                return false;
            }
            if ($('#altezza').val().length <= 0) {
                swa('Impostare altezza giocatore');
                return false;
            }
            if (typeof playerInfo['foot'] == 'undefined') {
                swa('Impostare piede preferito');
                return false;
            }
            if (typeof rolevalue[3][0] == 'undefined') {
                swa('Impostare ruolo preferito');
                return false;
            }

            $.ajax({
                type: 'POST',
                url: '{{ route('card.store') }}',
                dataType: 'json',
                data: {
                    info: JSON.stringify(playerInfo),
                    roles: [rolevalue[1], rolevalue[2], rolevalue[3]],
                    weakfoot: $('input[name=rating]:checked').val(),
                    skill: $('input[name=rating2]:checked').val(),
                    name: $('#name').val(),
                    surname: $('#surname').val(),
                    foot: $('.footchose.selected').attr('foot'),
                    overall: $('#overallnumber').text(),
                    heigth: $('#altezza').val(),
                    eta: $('#eta').val(),
                    weight: $('#peso').val(),
                    _token: '{{ csrf_token() }}'
                },
                success: function(data) {
                    if (data.result === true) {
                        Swal.fire({
                            title: 'Successo!',
                            text: data.message,
                            icon: 'success',
                            confirmButtonText: 'Continua'
                        }).then(function() {
                            location.reload();
                        });
                    }
                }
            });

        }

        function swa(msg) {
            Swal.fire({
                title: 'Errore!',
                text: msg,
                icon: 'error',
                confirmButtonText: 'Continua'
            });
        }
    </script>
@endsection
