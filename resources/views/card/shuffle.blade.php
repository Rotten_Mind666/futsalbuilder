@extends('layouts.app')
@section('css')
    @parent
    <style>
        .inline-div {
            display: inline-block !important;
        }

        .carddrag {
            cursor: move;
        }
    </style>
@endsection

@section('content')
    <div class="container-fluid px-5">
        <div class="row">
            <button class="btn btn-primary randomizeagain">RANDOMIZZA</button>
            <div class="col-12 bg-white rounded-3 text-white p-4"
                style="max-height: 85vh; min-height: 85vh; overflow-x: hidden !important;">
                <div class="form-group">
                    <label for="removeoverall" class="text-dark fw-bold">Nascondi overall</label>
                    <input type="checkbox" name="removeoverall" id="removeoverall">
                </div>
                <div class="row align-items-center">
                    <div class="col-6 border-right text-dark animate__animated animate__backInDown">
                        <h5>Team1</h5>
                        <b>Overall: <span class="overall1"></span></b>
                        <br>
                        <div class="inline-div">
                            <i class="fa-solid fa-shield text-primary fa-3x"></i>
                            <p class="team1def text-center fw-bold">fqw</p>
                        </div>
                        <div class="inline-div">
                            <i class="fa-solid fa-pen-ruler text-success fa-3x"`></i>
                            <p class="team1cen text-center fw-bold">fqw</p>
                        </div>
                        <div class="inline-div">
                            <i class="fa-solid fa-up-long text-danger fa-3x"></i>
                            <p class="team1att text-center fw-bold">fqw</p>
                        </div>
                        <div class="team1players">

                        </div>
                    </div>
                    <div class="col-6 text-dark animate__animated animate__backInDown">
                        <h5>Team2</h5>
                        <b>Overall: <span class="overall2"></span></b>
                        <br>
                        <div class="inline-div">
                            <i class="fa-solid fa-shield text-primary fa-3x"></i>
                            <p class="team2def text-center fw-bold">fqw</p>
                        </div>
                        <div class="inline-div">
                            <i class="fa-solid fa-pen-ruler text-success fa-3x"></i>
                            <p class="team2cen text-center fw-bold">fqw</p>
                        </div>
                        <div class="inline-div">
                            <i class="fa-solid fa-up-long text-danger fa-3x"></i>
                            <p class="team2att text-center fw-bold">fqw</p>
                        </div>
                        <div class="team2players">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    @parent
    <script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/dragula/4.4.3/dragula.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#removeoverall').on('click', function() {
                if($(this).is(':checked')){
                    $('.overall1').addClass('d-none');
                    $('.overall2').addClass('d-none');
                    $('.singleoverallplayer').addClass('d-none');
                } else {
                    $('.overall1').removeClass('d-none');
                    $('.overall2').removeClass('d-none');
                    $('.singleoverallplayer').removeClass('d-none');
                }
            });
            var overallteam1 = 0;
            var team1def = 0;
            var team1cen = 0;
            var team1att = 0;

            var overallteam2 = 0;
            var team2def = 0;
            var team2cen = 0;
            var team2att = 0;

            new Sortable(document.querySelector('.team1players'), {
                group: 'shared', // set both lists to same group
                animation: 150,
                swap: true, // Enable swap plugin
                swapClass: 'highlight',
                onSort: refreshList
            });

            new Sortable(document.querySelector('.team2players'), {
                group: 'shared',
                animation: 150,
                swap: true, // Enable swap plugin
                swapClass: 'highlight',
            });

            function refreshList() {
                var halfpartec = 0;


                var t1overall = 0;
                var t1dif = 0;
                var t1cen = 0;
                var t1att = 0;
                $('.team1players').children().each(function(key, objt1) {
                    halfpartec += 1;
                    t1overall += parseInt($(objt1).attr('overall'));
                    if ($(objt1).attr('ruolo') == 'ATTACCANTE') {
                        t1att += 1;
                    }
                    if ($(objt1).attr('ruolo') == 'CENTROCAMPISTA') {
                        t1cen += 1;
                    }
                    if ($(objt1).attr('ruolo') == 'DIFENSORE') {
                        t1dif += 1;
                    }
                });

                var t2overall = 0;
                var t2dif = 0;
                var t2cen = 0;
                var t2att = 0;
                $('.team2players').children().each(function(key, objt2) {
                    t2overall += parseInt($(objt2).attr('overall'));
                    if ($(objt2).attr('ruolo') == 'ATTACCANTE') {
                        t2att += 1;
                    }
                    if ($(objt2).attr('ruolo') == 'CENTROCAMPISTA') {
                        t2cen += 1;
                    }
                    if ($(objt2).attr('ruolo') == 'DIFENSORE') {
                        t2dif += 1;
                    }
                });

                $('.overall1').text((overallteam1 / halfpartec).toFixed(0));
                $('.overall2').text((overallteam2 / halfpartec).toFixed(0));

                $('.team1def').text(t1dif);
                $('.team1cen').text(t1cen);
                $('.team1att').text(t1att);
                $('.team2def').text(t2dif);
                $('.team2cen').text(t2cen);
                $('.team2att').text(t2att);
            }

            var halfpartec = 0;
            $($.parseJSON(getCookie('teams'))).each(function(key, obj) {
                $(obj.team1).each(function(keyt1, objt1) {
                    halfpartec += 1;
                    overallteam1 += parseInt(objt1.overall);
                    var whaticon = '';
                    if (objt1.ruolo == 'ATTACCANTE') {
                        team1att += 1;
                        whaticon = '<i class="fa-solid fa-up-long text-danger"></i>';
                    }
                    if (objt1.ruolo == 'CENTROCAMPISTA') {
                        team1cen += 1;
                        whaticon = '<i class="fa-solid fa-pen-ruler text-success"></i>';
                    }
                    if (objt1.ruolo == 'DIFENSORE') {
                        team1def += 1;
                        whaticon = '<i class="fa-solid fa-shield text-primary"></i>';
                    }

                    $(`<div class="card carddrag my-2" idplayer="` + objt1.id + `" ruolo="` + objt1
                        .ruolo + `" overall="` +
                        objt1.overall + `">
                        <div class="card-body">
                            <h5 class="card-title">` + objt1.name + ` ` + objt1.cognome +
                        ` <br><span class="text-primary fw-bold singleoverallplayer">` + parseFloat(objt1.overall).toFixed(0) + `</span></h5>
                            <p class="card-text">` + objt1.ruolo + ' ' + whaticon + `</p>
                        </div>
                    </div>
                    `).appendTo('.team1players');
                });
                $(obj.team2).each(function(keyt2, objt2) {
                    overallteam2 += parseInt(objt2.overall);
                    var whaticon = '';
                    if (objt2.ruolo == 'ATTACCANTE') {
                        team2att += 1;
                        whaticon = '<i class="fa-solid fa-up-long text-danger"></i>';
                    }
                    if (objt2.ruolo == 'CENTROCAMPISTA') {
                        team2cen += 1;
                        whaticon = '<i class="fa-solid fa-pen-ruler text-success"></i>';
                    }
                    if (objt2.ruolo == 'DIFENSORE') {
                        team2def += 1;
                        whaticon = '<i class="fa-solid fa-shield text-primary"></i>';
                    }

                    $(`<div class="card carddrag my-2" idplayer="` + objt2.id + `" ruolo="` + objt2
                        .ruolo + `" overall="` +
                        objt2.overall + `">
                        <div class="card-body">
                            <h5 class="card-title">` + objt2.name + ` ` + objt2.cognome +
                        ` <br><span class="text-primary fw-bold singleoverallplayer">` + parseFloat(objt2.overall).toFixed(0) + `</span></h5>
                            <p class="card-text">` + objt2.ruolo + ' ' + whaticon + `</p>
                        </div>
                    </div>
                    `).appendTo('.team2players');
                });
            });
            $('.overall1').text(parseFloat((overallteam1 / halfpartec).toFixed(0)));
            $('.overall2').text(parseFloat((overallteam2 / halfpartec).toFixed(0)));

            $('.team1def').text(team1def);
            $('.team1cen').text(team1cen);
            $('.team1att').text(team1att);
            $('.team2def').text(team2def);
            $('.team2cen').text(team2cen);
            $('.team2att').text(team2att);

            $('.randomizeagain').on('click', function() {
                var par = [];
                $('.carddrag').each(function(key, val) {
                    par.push($(val).attr('idplayer'));
                });
                $.ajax({
                    url: "{{ route('shuffle') }}",
                    data: {
                        'squad': par,
                        'maxcristiani': ($.parseJSON(getCookie('teams')).team1.length + $.parseJSON(getCookie('teams')).team2.length) / 2
                    },
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type: 'POST',
                    dataType: 'json',
                    success: function(result) {
                        setCookie('teams', result.teams, 9999);
                        location.href = result.route;
                    }
                });
            });
        });
    </script>
@endsection
