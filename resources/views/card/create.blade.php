@extends('layouts.app')
@section('css')
    @parent
@endsection
@section('content')
    <div class="container rounded px-5">
        <div class="row">
            <div class="card mb-3">
                <div class="row g-0">
                    <div class="col-md-4">
                        <canvas id="donutChart"></canvas>
                    </div>
                    <div class="col-md-8">
                        <div class="card-body">

                            <form action="{{ route('card.store') }}" method="post">
                                <div class="row">
                                    <div class="col-6">Nome <input required type="text" name="name"
                                            class="form-control"></div>
                                    <div class="col-6">Cognome <input type="text" name="cognome" class="form-control">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-group my-3">
                                        <span class="input-group-text" id="basic-addon1">Ruolo</span>
                                        <select name="ruolo" id="ruolo" class="form-control">
                                            <option value="difensore">Difensore</option>
                                            <option value="centrocampista">Centrocampista</option>
                                            <option value="attaccante">Attaccante</option>
                                        </select>
                                    </div>
                                </div>
                                @csrf
                                @php
                                    $role = ['Velocita', 'Tiro', 'Passaggio', 'Dribbling', 'Difesa', 'Fisico'];
                                    foreach ($role as $value) {
                                        echo '<div class="input-group my-3">
                                        <span class="input-group-text" id="basic-addon1">' .
                                            $value .
                                            '</span>
                                        <input name="'.strtolower($value).'" id="skill'.
                                            strtolower($value) .
                                            '" type="number" value="1" min="1" max="100" class="form-control" placeholder="' .
                                            $value .
                                            '" aria-label="' .
                                            $value .
                                            '" aria-describedby="basic-addon1" onchange="updateChart()">
                                    </div>';
                                    }
                                @endphp
                                <div class="text-center">
                                    <button type="submit" class="btn btn-dark">CREA GIOCATORE</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    @parent
    <script>
        var ctx = document.getElementById('donutChart').getContext('2d');

        // Inizializza il grafico con valori di default
        const data = {
            labels: [
                'Velocita',
                'Tiro',
                'Passaggio',
                'Dribbling',
                'Difesa',
                'Fisico'
            ],
            datasets: [{
                data: [1, 1, 1, 1, 1, 1], // Valori di default
                fill: true,
                backgroundColor: 'rgba(255, 99, 132, 0.2)',
                borderColor: 'rgb(255, 99, 132)',
                pointBackgroundColor: 'rgb(255, 99, 132)',
                pointBorderColor: '#fff',
                pointHoverBackgroundColor: '#fff',
                pointHoverBorderColor: 'rgb(255, 99, 132)'
            }]
        };
        var myChart = new Chart(ctx, {
            type: 'radar',
            data: data,
            options: {
                scales: {
                    y: {
                        beginAtZero: true, // Inizia l'asse y da zero
                        min: 0, // Limite minimo
                        max: 100 // Limite massimo
                    }
                },
                legend: {
                    display: false
                },
                scales: {
                    x: {
                        display: false
                    },
                    y: {
                        display: false
                    }
                }
            }
        });

        // Funzione per aggiornare il dataset del grafico
        function updateChart() {
            // Ottieni i valori dagli input
            var updatedData = [
                $('#skillvelocita').val(),
                $('#skilltiro').val(),
                $('#skillpassaggio').val(),
                $('#skilldribbling').val(),
                $('#skilldifesa').val(),
                $('#skillfisico').val()
            ];

            // Aggiorna il dataset del grafico con i nuovi valori
            myChart.data.datasets[0].data = updatedData;

            // Aggiorna il grafico
            myChart.update();
        }
    </script>
@endsection
