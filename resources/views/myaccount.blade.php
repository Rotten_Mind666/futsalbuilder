<?php
use App\Models\User;
$alluser = User::where('id', '!=', Auth::user()->id)->get();
?>
@extends('layouts.app')
@section('css')
    @parent
    <style>
        .bg-field {
            background-image: url('fld.jpg');
            background-position: center;
            background-size: cover;
            background-repeat: no-repeat;
        }
    </style>
@endsection

@section('content')
    <div class="container-fluid px-5">
        <div class="row">
            <div class="col-12 bg-field rounded-3 text-white p-4"
                style="max-height: 85vh; min-height: 85vh; overflow-x: hidden !important;">
                <div class="row bg-white mx-0 p-2">
                    <div class="col-4">
                        <button type="button" class="btn btn-warning condividiconbtn" disabled data-bs-toggle="modal"
                            data-bs-target="#exampleModal">
                            CONDIVIDI
                        </button>
                    </div>
                    <div class="modal fade text-dark" id="exampleModal" aria-labelledby="exampleModalLabel"
                        aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Seleaziona a chi condividere:</h5>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal"
                                        aria-label="Close"></button>
                                </div>
                                <div class="modal-body">
                                    <select style="width: 100%" class="associateuser" name="associateto[]" id="associateto"
                                        multiple="multiple">
                                        @foreach ($alluser as $user)
                                            <option value="{{ $user->id }}">{{ $user->name . ' - ' . $user->email }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-primary savecondivisione">Salva</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    @include('modaledit')
                </div>

                <table class="table table-dark table-striped">
                    <thead>
                        <tr>
                            <th><input class="allcheck" type="checkbox" onclick="clickallcheck(this)"></th>
                            <th>Foto</th>
                            <th>Nome</th>
                            <th>Cognome</th>
                            <th>Overall</th>
                            <th>Ruolo</th>
                            <th>Velocita</th>
                            <th>Tiro</th>
                            <th>Difesa</th>
                            <th>Fisico</th>
                            <th>Passaggio</th>
                            <th>Dribbling</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($pl as $p)
                            <tr style="cursor: pointer;" onclick="clickCheckbox(this)">
                                <td><input class="{{ $p->id }}checkbox singlecheck"
                                        name="{{ $p->id }}checkbox" type="checkbox" idplayer="{{ $p->id }}">
                                </td>
                                <td>{{ $p->img }}</td>
                                <td>{{ $p->name }}</td>
                                <td>{{ $p->cognome }}</td>
                                <td>{{ round($p->overall, 0) }}</td>
                                <td>{{ strtoupper($p->ruolo) }}</td>
                                <td>{{ $p->vel }}</td>
                                <td>{{ $p->tir }}</td>
                                <td>{{ $p->dif }}</td>
                                <td>{{ $p->fis }}</td>
                                <td>{{ $p->pas }}</td>
                                <td>{{ $p->dri }}</td>
                                <td>
                                    <button type="button" class="btn btn-light editbutton" data-bs-toggle="modal"
                                        fullstat="{{ $p->id }}" data-bs-target="#editModal"
                                        roleplayer="{{ $p->ruolo }}" nameplayer="{{ $p->name }}"
                                        surnameplayer="{{ $p->cognome }}"
                                        allstatplayer="{{ 'velocita_' . $p->vel . '-' . 'tiro_' . $p->tir . '-' . 'difesa_' . $p->dif . '-' . 'fisico_' . $p->fis . '-' . 'passaggio_' . $p->pas . '-' . 'dribbling_' . $p->dri }}"
                                        namesurnameplayer="{{ $p->name . ' ' . $p->cognome }}">
                                        Modifica
                                    </button>
                                    <form action="{{ route('card.delete') }}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <input type="hidden" name="id" value="{{$p->id}}">
                                        <button type="submit" class="btn btn-danger"
                                            onclick="return confirm('Sei sicuro di voler eliminare questo record?')">Elimina</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    @parent
    <script>
        $(document).ready(function() {
            $('.associateuser').select2({
                dropdownParent: $('#exampleModal')
            });

            $('.editbutton').on('click', function() {
                $($(this).attr('allstatplayer').split('-')).each(function(key, val) {
                    var spl = val.split('_');
                    $('#skill' + spl[0]).val(spl[1]);
                });
                $('.fullstat').val($(this).attr('fullstat'));
                $('.roleplayermodal').val($(this).attr('roleplayer').toLowerCase()).trigger('change');
                $('.namemodaledit').val($(this).attr('nameplayer'));
                $('.cognomemodaledit').val($(this).attr('surnameplayer'));
                $('.nameplayermodal').text($(this).attr('namesurnameplayer'));

                updateChart();
            });

            $('.savecondivisione').on('click', function() {
                var playerarray = [];
                $('.singlecheck[type="checkbox"]:checked').each(function(key, val) {
                    var kk = $(val).attr('idplayer');
                    playerarray.push(kk);
                });
                var ownerid = $('.associateuser').val();

                if ($(ownerid).length <= 0) {
                    Swal.fire({
                        title: 'Errore!',
                        text: 'Selezionare almeno un utente per associare i players!',
                        icon: 'error',
                        confirmButtonText: 'Ok'
                    });
                    return false;
                }
                $.ajax({
                    url: "{{ route('sharewith') }}",
                    data: {
                        'sharedlist': playerarray,
                        'shareowner': ownerid
                    },
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type: 'POST',
                    dataType: 'json',
                    success: function(result) {
                        Swal.fire({
                            title: 'Evviva!',
                            text: 'Tutti i players scelti sono stati associati correttamente!',
                            icon: 'success',
                            confirmButtonText: 'Ok'
                        }).then((result) => {
                            location.reload();
                        });
                    }
                });
            });
        });

        function clickallcheck(that) {
            if ($(that).is(':checked')) {
                $('.singlecheck:not(:checked)').click();
            } else {
                $('.singlecheck:checked').click();
            }
            checkifoneischecked();
        }

        function clickCheckbox(that) {
            $($(that).children()[0]).children()[0].click();
            checkifoneischecked();
        }

        function checkifoneischecked() {
            if ($('[type="checkbox"]:checked').length > 0) {
                $('.condividiconbtn').prop('disabled', false);
            } else {
                $('.condividiconbtn').prop('disabled', true);
            }
        }
    </script>
@endsection
