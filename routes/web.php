<?php

use App\Models\Player;
use App\Models\PlayerShare;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Auth::routes();
Route::get('/', function () {
    $myplayer = PlayerShare::where('idowner', Auth::user()->id)->pluck('idplayer')->toArray();
    $pl = Player::whereIn('id', $myplayer)->get();
    return view('welcome', compact('pl'));
})->name('home')->middleware('auth');

Route::get('/myaccount', [App\Http\Controllers\PlayerController::class, 'myaccount'])->name('myaccount')->middleware('auth');
Route::post('/sharewith', [App\Http\Controllers\PlayerController::class, 'sharewith'])->name('sharewith')->middleware('auth');

Route::get('/card/create', [App\Http\Controllers\PlayerController::class, 'create'])->name('card.create')->middleware('auth');
Route::post('/card/store', [App\Http\Controllers\PlayerController::class, 'store'])->name('card.store')->middleware('auth');
Route::post('/card/edit', [App\Http\Controllers\PlayerController::class, 'edit'])->name('card.edit')->middleware('auth');
Route::delete('/card/delete', [App\Http\Controllers\PlayerController::class, 'delete'])->name('card.delete')->middleware('auth');
Route::post('/shuffle', [App\Http\Controllers\PlayerController::class, 'shuffle'])->name('shuffle')->middleware('auth');
Route::get('/shuffleview', [App\Http\Controllers\PlayerController::class, 'shuffleview'])->name('card.shuffleview')->middleware('auth');
